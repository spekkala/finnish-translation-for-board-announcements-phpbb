<?php
/**
*
* Board Announcements extension for the phpBB Forum Software package.
* Finnish translation by Sami Pekkala (https://bitbucket.org/spekkala/finnish-translation-for-board-announcements-phpbb)
*
* @copyright (c) 2014 phpBB Limited <https://www.phpbb.com>
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, array(
	'BOARD_ANNOUNCEMENTS_SETTINGS'			=> 'Keskustelupalstan tiedotteen asetukset',
	'BOARD_ANNOUNCEMENTS_SETTINGS_EXPLAIN'	=> 'Tässä voit luoda ja muokata tiedotetta, joka näkyy kaikilla keskustelupalstasi sivuilla.',

	'BOARD_ANNOUNCEMENTS_ENABLE'			=> 'Näytä tiedote',
	'BOARD_ANNOUNCEMENTS_USERS'				=> 'Käyttäjät, joille tiedote näytetään',
	'BOARD_ANNOUNCEMENTS_DISMISS'			=> 'Salli käyttäjien sulkea tiedote',

	'BOARD_ANNOUNCEMENTS_EVERYONE'			=> 'Kaikki',

	'BOARD_ANNOUNCEMENTS_BGCOLOR'			=> 'Tiedotteen taustaväri',
	'BOARD_ANNOUNCEMENTS_BGCOLOR_EXPLAIN'	=> 'Syötä tiedotteen taustaväri heksadesimaalimuodossa (esim. FFFF80). Jätä kenttä tyhjäksi käyttääksesi oletusväriä.',

	'BOARD_ANNOUNCEMENTS_EXPIRY'			=> 'Tiedotteen umpeutumisajankohta',
	'BOARD_ANNOUNCEMENTS_EXPIRY_EXPLAIN'	=> 'Aseta ajankohta, jolloin tiedote poistuu näkyvistä. Jätä kenttä tyhjäksi, jos et halua tiedotteen umpeutuvan.',
	'BOARD_ANNOUNCEMENTS_EXPIRY_INVALID'	=> 'Umpeutumisajankohta oli virheellinen tai menneisyydessä.',
	'BOARD_ANNOUNCEMENTS_EXPIRY_FORMAT'		=> 'YYYY-MM-DD HH:MM',

	'BOARD_ANNOUNCEMENTS_TEXT'				=> 'Tiedotteen sisältö',
	'BOARD_ANNOUNCEMENTS_PREVIEW'			=> 'Keskustelupalstan tiedote – Esikatselu',

	'BOARD_ANNOUNCEMENTS_UPDATED'			=> 'Keskustelupalstan tiedote on päivitetty.',
));
