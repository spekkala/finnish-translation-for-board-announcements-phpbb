See below for English description.

# Suomennos Board Announcements -laajennukselle

Suomenkielinen kielipaketti phpBB:n Board Announcements -laajennukselle.

## Asennus

1. Asenna Board Announcements -laajennus ensin.

2. Lataa laajennuksen versiota vastaava kielipaketti alla olevasta luettelosta:

	- [1.0.6](https://bitbucket.org/spekkala/finnish-translation-for-board-announcements-phpbb/downloads/board-announcements-fi-1.0.6.zip)
	- [1.0.5](https://bitbucket.org/spekkala/finnish-translation-for-board-announcements-phpbb/downloads/board-announcements-fi-1.0.5.zip)
	- [1.0.4](https://bitbucket.org/spekkala/finnish-translation-for-board-announcements-phpbb/downloads/board-announcements-fi-1_0_4.7z)

3. Pura paketin sisältö phpBB:n `ext/phpbb/boardannouncements`-hakemistoon.

# Finnish Translation for Board Announcements

A Finnish language pack for the Board Announcements extension for phpBB.

## Installation

1. Install the Board Announcements extension first.

2. Download the language pack matching the version of the extension from the
list below:

	- [1.0.6](https://bitbucket.org/spekkala/finnish-translation-for-board-announcements-phpbb/downloads/board-announcements-fi-1.0.6.zip)
	- [1.0.5](https://bitbucket.org/spekkala/finnish-translation-for-board-announcements-phpbb/downloads/board-announcements-fi-1.0.5.zip)
	- [1.0.4](https://bitbucket.org/spekkala/finnish-translation-for-board-announcements-phpbb/downloads/board-announcements-fi-1_0_4.7z)

3. Extract the contents of the archive into the `ext/phpbb/boardannouncements`
directory under your phpBB root directory.
