# Change Log

## 1.0.6 (2018-01-01)

- Update translations to be compatible with Board Announcements 1.0.6.

## 1.0.5 (2017-02-26)

- Update translations to be compatible with Board Announcements 1.0.5.

## 1.0.4 (2015-10-06)

- Initial release.
